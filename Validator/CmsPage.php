<?php

namespace GetNoticed\RequireLogin\Validator;

use Magento\Framework;
use Magento\Cms;
use GetNoticed\RequireLogin as RL;
use Psr\Log;

/**
 * Class CmsPage
 *
 * @package GetNoticed\RequireLogin\Validator
 */
class CmsPage
    implements RL\Validators\ValidatorInterface
{

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $cmsPageFactory;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Page
     */
    protected $cmsPageResource;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        Cms\Model\PageFactory $cmsPageFactory,
        Cms\Model\ResourceModel\Page $cmsPageResource,
        Framework\App\RequestInterface $request,
        Log\LoggerInterface $logger
    ) {
        $this->cmsPageFactory = $cmsPageFactory;
        $this->cmsPageResource = $cmsPageResource;
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getControllerNames(): array
    {
        return [
            \Magento\Cms\Controller\Page\View::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): int
    {
        return 100;
    }

    /**
     * @inheritDoc
     */
    public function validate(
        \Magento\Framework\App\Action\Action $controller,
        \Magento\Framework\App\Request\Http $request
    ): bool {
        /** @var \Magento\Cms\Controller\Page\View $controller */
        $pageId = $this->request->getParam('page_id', $this->request->getParam('id', false));
        /** @var \Magento\Cms\Model\Page $cmsPage */
        $cmsPage = $this->cmsPageFactory->create();
        $this->cmsPageResource->load($cmsPage, $pageId);

        // If the page does not exist, deny access to be sure.
        if ($cmsPage->getId() === null) {
            $this->logger->debug('[RequireLogin->CmsPage] CMS page with ID "%s" does not exist', $pageId);

            return false;
        }

        $isAllowed = $cmsPage->getData('is_public') == '1';

        $this->logger->debug(
            sprintf(
                '[RequireLogin->CmsPage] CMS page "%s" allowed: %s',
                $cmsPage->getId(),
                $isAllowed ? 'Yes' : 'No'
            )
        );

        return $isAllowed;
    }

}