<?php

namespace GetNoticed\RequireLogin\Validator;

use GetNoticed\RequireLogin as RL;
use Psr\Log;

/**
 * Class ActionHandle
 *
 * @package GetNoticed\RequireLogin\Validator
 */
class ActionHandle
    implements RL\Validators\ValidatorInterface
{

    // DI

    /**
     * @var Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var RL\Helper\AllowedActionHandles
     */
    protected $allowedActionHandles;

    /**
     * @inheritDoc
     */
    public function __construct(
        Log\LoggerInterface $logger,
        RL\Helper\AllowedActionHandles $allowedActionHandles
    ) {
        $this->logger = $logger;
        $this->allowedActionHandles = $allowedActionHandles;
    }

    /**
     * @inheritDoc
     */
    public function getControllerNames(): array
    {
        return [
            \Magento\Framework\App\Action\Action::class
        ];
    }

    /**
     * Very high priority, because this must always be the last one to run.
     *
     * @return int
     */
    public function getPriority(): int
    {
        return 999999;
    }

    /**
     * @inheritDoc
     */
    public function validate(
        \Magento\Framework\App\Action\Action $controller,
        \Magento\Framework\App\Request\Http $request
    ): bool {
        $isAllowed = in_array(
            strtolower($request->getFullActionName()),
            array_values($this->allowedActionHandles->getAllowedActionHandles())
        );

        $this->logger->debug(
            sprintf(
                '[RequireLogin->ActionHandle] Checking if %s matches any of: %s (response: %s)',
                $request->getFullActionName(),
                implode(', ', $this->allowedActionHandles->getAllowedActionHandles()),
                $isAllowed ? 'Yes' : 'No'
            )
        );

        return $isAllowed;
    }

}
