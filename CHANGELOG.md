# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
* [BAM-25](http://jira.getnoticed.nl:8080/browse/BAM-25) : Create a module which can deny everyone access and whitelist specific pages
