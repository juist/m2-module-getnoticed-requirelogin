<?php

namespace GetNoticed\RequireLogin\Helper;

use Magento\Store;

/**
 * Class ConfigHelper
 *
 * @package GetNoticed\RequireLogin\Helper
 */
class ConfigHelper
    extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_BASE = 'getnoticed_requirelogin/settings/%s';
    const XML_PATH_ENABLED = 'enabled';
    const XML_PATH_HOMEPAGE_RESTRICTED = 'restrict_cms_index';

    /**
     * @return bool
     */
    public function isModuleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            sprintf(self::XML_PATH_BASE, self::XML_PATH_ENABLED),
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * @return bool
     */
    public function isHomepageRestricted(): bool
    {
        return $this->scopeConfig->isSetFlag(
            sprintf(self::XML_PATH_BASE, self::XML_PATH_HOMEPAGE_RESTRICTED),
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }
}