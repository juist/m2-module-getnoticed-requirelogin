<?php

namespace GetNoticed\RequireLogin\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class AllowedActionHandles
 * @package GetNoticed\RequireLogin\Helper
 */
class AllowedActionHandles
    extends AbstractHelper
{

    /**
     * @var array
     */
    protected $allowedActionHandles = [];

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * AllowedActionHandles constructor.
     * @param Context $context
     * @param ConfigHelper $configHelper
     * @param array $handles
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        array $handles = []
    ) {
        $this->allowedActionHandles = $handles;
        $this->configHelper = $configHelper;

        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getAllowedActionHandles(): array
    {
        if($this->configHelper->isHomepageRestricted()){
            unset($this->allowedActionHandles['cms_index_index']);
        }

        return $this->allowedActionHandles;
    }
}