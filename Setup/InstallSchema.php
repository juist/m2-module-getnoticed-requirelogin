<?php

namespace GetNoticed\RequireLogin\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package GetNoticed\RequireLogin\Setup
 */
class InstallSchema
    implements InstallSchemaInterface
{

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // Start setup
        $installer = $setup;
        $installer->startSetup();
        $adapter = $installer->getConnection();

        // Add a column to the cms_page table indicating the page status
        $installer->getConnection()->addColumn(
            $adapter->getTableName('cms_page'),
            'is_public',
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Is page public'
            ]
        );

        // Finish setup
        $installer->endSetup();
    }

}