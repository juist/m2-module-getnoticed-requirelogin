<?php

namespace GetNoticed\RequireLogin\Observer;

use Magento\Framework;
use Magento\Customer;
use GetNoticed\RequireLogin as RL;
use Psr\Log;

/**
 * Class CheckRequireLogin
 *
 * @package GetNoticed\RequireLogin\Observer
 */
class CheckRequireLogin
    implements Framework\Event\ObserverInterface
{

    /**
     * @var Framework\App\Action\Action
     */
    protected $controller;

    /**
     * @var Framework\App\Request\Http
     */
    protected $request;

    // DI

    /**
     * @var Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \GetNoticed\RequireLogin\Validators\ValidatorList
     */
    protected $validatorList;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;

    /**
     * @var \GetNoticed\RequireLogin\Helper\ConfigHelper
     */
    protected $configHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
        Log\LoggerInterface $logger,
        RL\Validators\ValidatorList $validatorList,
        Framework\App\ResponseInterface $response,
        Customer\Model\Url $customerUrl,
        RL\Helper\ConfigHelper $configHelper,
        Customer\Model\Session $customerSession,
        Framework\UrlInterface $url,
        Framework\Message\ManagerInterface $messageManager
    ) {
        $this->logger = $logger;
        $this->validatorList = $validatorList;
        $this->response = $response;
        $this->customerUrl = $customerUrl;
        $this->configHelper = $configHelper;
        $this->customerSession = $customerSession;
        $this->url = $url;
        $this->messageManager = $messageManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Framework\Event\Observer $observer)
    {

        // If the require login is disabled, do not trigger observer!
        if (!$this->configHelper->isModuleEnabled()) {
            $this->logger->debug('[RequireLogin] Module not enabled');

            return;
        }

        // We also do not need to check if a customer is logged in.
        if ($this->customerSession->isLoggedIn()) {
            $this->logger->debug('[RequireLogin] Customer already logged in');

            return;
        }

        // Loop through all validators. By default, no one has access.
        // If a validator returns true, specific element has been whitelisted and we'll display the page.
        $this->controller = $observer->getData('controller_action');
        $this->request = $observer->getData('request');

        // Process validators and see if any of them whitelist this specific page.
        foreach ($this->validatorList->getValidators() as $validator) {
            // By default, do not trigger a validator
            // If there are no filters for action name, trigger the validator
            $bValidate = false;

            $this->logger->debug(sprintf('[RequireLogin] [Validator %s] Checking...', get_class($validator)));

            if (count($validator->getControllerNames()) == 0) {
                $bValidate = true;
                $this->logger->debug(sprintf('[RequireLogin] [Validator %s] All controllers', get_class($validator)));
            } else {
                foreach ($validator->getControllerNames() as $controllerName) {
                    if ($this->controller instanceof $controllerName
                        || \is_subclass_of($this->controller, $controllerName)) {
                        // Or if a controller matches, trigger the validator
                        $bValidate = true;
                        $this->logger->debug(
                            sprintf(
                                '[RequireLogin] [Validator %s] Matched controller %s',
                                get_class($validator),
                                $controllerName
                            )
                        );
                        break;
                    }
                }
            }

            // Must we validate?
            if ($bValidate === true && $validator->validate($this->controller, $this->request) === true) {
                // Page is allowed, do not process further.
                $this->logger->debug(sprintf('[RequireLogin] [Validator %s] Page is allowed', get_class($validator)));

                return;
            } else {
                $this->logger->debug(sprintf('[RequireLogin] [Validator %s] Page is denied', get_class($validator)));
            }
        }

        // If we reach this point, access is not allowed.
        // If it's an AJAX-loaded page, trigger the 404 page.
        // Otherwise, redirect to login page + redirect user back to this page afterwards.

        if ($this->request->isAjax()) {
            throw new Framework\Exception\NotFoundException(
                __('No access')
            );
        } else {
            $this->customerSession->setAfterAuthUrl($this->url->getUrl('*/*/*', $this->request->getParams()));
            $this->customerSession->authenticate();
        }
    }

}