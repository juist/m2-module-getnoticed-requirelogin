<?php

namespace GetNoticed\RequireLogin\Source\Options;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class CmsPageVisibility
 *
 * @package GetNoticed\RequireLogin\Config\Source
 */
class CmsPageVisibility
    implements OptionSourceInterface
{

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 0,
                'label' => 'Restricted (visible for logged in users only)'
            ],
            [
                'value' => 1,
                'label' => 'Public (visible for everyone)'
            ]
        ];
    }

}