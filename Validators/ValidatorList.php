<?php

namespace GetNoticed\RequireLogin\Validators;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class ValidatorList
 *
 * @package GetNoticed\RequireLogin\Validators
 */
class ValidatorList
    implements ValidatorListInterface
{

    /**
     * @var \GetNoticed\RequireLogin\Validators\ValidatorInterface[]
     */
    protected $validators = [];

    /**
     * ValidatorList constructor.
     *
     * @param array $validators
     */
    public function __construct(
        array $validators = []
    ) {
        foreach ($validators as $validator) {
            if (!$validator instanceof ValidatorInterface) {
                throw new LocalizedException(__('All validators should implement the proper interface'));
            }
        }

        $this->validators = $this->sortOrder($validators);
    }

    /**
     * @param array $validators
     *
     * @return array
     */
    public function sortOrder(array $validators)
    {
        $orderedArray = [];

        foreach ($validators as $validator) {
            /** @var \GetNoticed\RequireLogin\Validators\ValidatorInterface $validator */
            if ($validator->getPriority() < 1) {
                throw new LocalizedException(__('Priority must be 1 or  greater'));
            }

            $orderedArray[$validator->getPriority()] = $validator;
        }

        ksort($orderedArray, SORT_NUMERIC);

        return $orderedArray;
    }

    /**
     * @return \GetNoticed\RequireLogin\Validators\ValidatorInterface[]
     */
    public function getValidators(): array
    {
        return $this->validators;
    }

}