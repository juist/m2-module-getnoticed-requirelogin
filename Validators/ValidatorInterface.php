<?php

namespace GetNoticed\RequireLogin\Validators;

/**
 * Interface ValidatorInterface
 *
 * @package GetNoticed\RequireLogin\Validators
 */
interface ValidatorInterface
{

    /**
     * @return array
     */
    public function getControllerNames(): array;

    /**
     * A higher priority will result in being executed last.
     *
     * @return int
     */
    public function getPriority(): int;

    /**
     * @param \Magento\Framework\App\Action\Action $controller
     * @param \Magento\Framework\App\Request\Http  $request
     *
     * @return bool
     */
    public function validate(
        \Magento\Framework\App\Action\Action $controller,
        \Magento\Framework\App\Request\Http $request
    ): bool;

}