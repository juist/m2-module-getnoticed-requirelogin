<?php

namespace GetNoticed\RequireLogin\Validators;

/**
 * Interface ValidatorListInterface
 *
 * @package GetNoticed\RequireLogin\Validators
 */
interface ValidatorListInterface
{

    /**
     * @return \GetNoticed\RequireLogin\Validators\ValidatorInterface
     */
    public function getValidators(): array;

}